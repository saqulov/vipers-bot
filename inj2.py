import discord

TOKEN = 'NzAxNTIxNTk5MDU2MDUyMjc2.XpytsQ.jCVGXoQ7bstugIBGiYPhlbI99Ko'
ABBREVIATIONS = {
     'aga': 'Ace Green Arrow', 'aww': 'Amazon Wonder Woman', 'aqm': 'Aquaman', 'akbm': 'Arkham Knight Batman',
     'aaam': 'At. Armor Aquaman', 'asm': 'Armored Superman', 'atr': 'Atrocitus', 'bane': 'Bane', 'sbm': 'Batman',
     'bc': 'Black Canary', 'bm': 'Black Manta', 'bmr': 'Blade Master Robin', 'bb': 'Blue Beetle', 'bnbm': 'Batman Ninja Batman',
     'bncw': 'Batman Ninja Catwoman', 'bngg': 'Batman Ninja Gorilla Grodd', 'bnhq': 'Batman Ninja Harley Quinn', 'bnlj': 'Batman Ninja Lord Joker',
     'bnr': 'Batman Ninja Robin', 'bra': 'Brainiac', 'cc': 'Captain Cold', 'cw': 'Catwoman', 'ch': 'Cheetah', 'cbm': 'Classic Batman',
     'csm': 'Classic Superman', 'cyb': 'Cyborg', 'dsg': 'Dark Supergirl', 'dks': 'Darkseid', 'ds': 'Deadshot', 'df': 'Doctor Fate',
     'egl': 'Emerald Green Lantern', 'enc': 'Enchantress', 'esf': 'Energized Starfire', 'eb': 'Enranged Bane', 'ebane': 'Enranged Bane',
     'epi': 'Entangling Poison Ivy', 'fs': 'Firestorm', 'fpi': 'Flora Poison Ivy', 'gg': 'Gorilla Grodd', 'ga': 'Green Arrow', 'gl': 'Green Lantern',
     'gr': 'grid', 'hq': 'Harley Quinn', 'hbhq': 'Heartbreaker Harley Quinn', 'hsc': 'Horrific Scarecrow', 'hb': 'Hellboy', 'jlam': 'Justice League Aquaman',
     'jlbm': 'Justice League Batman', 'jlc': 'Justice League Cyborg', 'jltf': 'Justice League The Flash', 'jlsm': 'Justice League Superman',
     'jsgl': 'John Stewart Green Lantern', 'jk': 'Joker', 'koaam': 'King of Atlantis Aquaman', 'llj': 'Last Laugh Joker', 'kbm': 'Knightmare Batman',
     'mtc': 'M. Thief Catwoman', 'mvasg': 'Multiverse Armored Supergirl', 'mvbw': 'Multiverse Batwoman', 'mvbl': 'Multiverse Black Lightning',
     'mvcc': 'Multiverse Captain Cold', 'mvtf': 'Multiverse The Flash', 'mvga': 'Multiverse Green Arrow',
     'mvsg': 'Multiverse Super Girl', 'mvwc': 'Multiverse White Canary', 'mww': 'Mythic Wonder Woman', 'nw': 'Nightwing', 'rdn': 'Raiden',
     'psg': 'Powered Supergirl', 'pg': 'Powergirl', 'pbm': 'Predator Batman', 'pst': 'Primal Swamp Thing', 'rh': 'Red Hood', 'rf': 'Reverse Flash',
     'robin': 'Robin', 'sc': 'Scarecrow', 'shz': 'Shazam', 'sb': 'Silver Banshee', 'sbc': 'Sonic Black Canary', 'ssdf': 'Soulstealer Doctor Fate',
     'sftf': 'Speed Force The Flash', 'sz': 'Sub-Zero', 'ssds': 'Suicide Squad Deadshot', 'sshq': 'Suicide Squad Harley Quinn', 'sm': 'Superman',
     'st': 'Swamp Thing', 'tf': 'The Flash', 'ubc': 'Unbreakable Cyborg', 'uhq': 'Unhinged Harley Quinn', 'wqww': 'War Queen Wonder Woman', 'ww': 'Wonder Woman',
}

PASSIVES = {
    'aga': ['As the tags out,Green Arrow fires an arrow that temporary stops his opponent\'s power generation.']
}

SUPERMOVES = {
    'aga': 'Green Arrow unloads arrows of fire and ice onto his victim.'
}

def findout_abbr(parameter):
    return ABBREVIATIONS[parameter]

def findout_passives(parameter):
    return PASSIVES[parameter][0]

def findout_super(parameter):
    return SUPERMOVES[parameter]

client = discord.Client()

@client.event
async def on_ready():
    print(f'{client.user.name} has connected to Discord!')

@client.event
async def on_member_join(member):
    await member.create_dm()
    await member.dm_channel.send(
        f'Hi {member.name}, welcome to the Vipers League!\nPlease, read rules in the chat with corresponding name.\n Contact to haitodo#7394, if you haven\'t yet.'
    )

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    msg = message.content.lower().split(' ')
    if msg[0] == "acronym":
        response = findout_abbr(msg[1])
        await message.channel.send(response)
    elif msg[0] == "passives":
        response = findout_passives(msg[1])
        await message.channel.send(response)
    elif msg[0] == "supermove":
        response = findout_super(msg[1])
        await message.channel.send(response)
    elif msg[0] == "talents":
        await message.channel.send(file=discord.File('talents.png'))

client.run(TOKEN)
